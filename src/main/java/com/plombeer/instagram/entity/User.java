package com.plombeer.instagram.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.plombeer.instagram.entity.enums.UserRoleEnum;
import com.sun.istack.internal.NotNull;
import org.springframework.security.core.userdetails.UserDetails;

import javax.persistence.*;
import java.security.cert.Extension;
import java.util.Date;
import java.util.EnumSet;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by Никита on 04.06.2016.
 */
@Entity
@Table(name = "users")
@NamedNativeQueries({
        @NamedNativeQuery(name = "getSubscriptions", query = "SELECT * FROM subscribers WHERE sub = ?"),
        @NamedNativeQuery(name = "getSubscribers", query = "SELECT * FROM subscribers WHERE usr = ?"),
        @NamedNativeQuery(name = "subscribe", query = "INSERT INTO subscribers (usr, sub) VALUES (?, ?)"),
        @NamedNativeQuery(name = "hasAlreadySubscribe", query = "SELECT COUNT(*) FROM subscribers  WHERE (usr = ?) AND (sub = ?)"),
        @NamedNativeQuery(name = "getFeed", query = "SELECT * FROM photos WHERE photos.author_username in (SELECT subscribers.usr FROM subscribers WHERE (subscribers.sub = ?)) ORDER BY createdOn desc limit ?, 10"),
        @NamedNativeQuery(name = "unsubscribe", query = "DELETE FROM subscribers WHERE (usr = ?) AND (sub = ?)")
    }
)
public class User implements UserDetails{


    @Id
    @Column(name = "username", unique = true, length = 50)
    private String username;

    @Column(name = "name")
    private String name;

    @JsonIgnore
    @Column(name = "password")
    private String password;

    @Column(name = "email", unique = true)
    private String email;

    @Column(name = "createdOn")
    private long createdOn;

    @Column(name = "updatedOn")
    private long updatedOn;

    @JsonIgnore
    @Column(name = "role")
    private UserRoleEnum userRole;

    @Transient
    private long expires;

    @NotNull
    private boolean accountExpired;

    @NotNull
    private boolean accountLocked;

    @NotNull
    private boolean credentialsExpired;

    @NotNull
    private boolean accountEnabled;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "user", fetch = FetchType.EAGER, orphanRemoval = true)
    private Set<UserAuthority> authorities;

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name="subscribers",
            joinColumns = @JoinColumn(name="usr", referencedColumnName="username"),
            inverseJoinColumns = @JoinColumn(name="sub", referencedColumnName="username"))
    @JsonIgnore
    private Set<User> subscribers;

    @OneToOne(fetch = FetchType.EAGER, cascade = CascadeType.MERGE)
    private Document avatar;

    public User() {
    }

    public User(String username) {
        this.username = username;
    }

    public User(String username, Date expires) {
        this.username = username;
        this.expires = expires.getTime();
    }

    @Override
    @JsonIgnore
    public Set<UserAuthority> getAuthorities() {
        return authorities;
    }

    @JsonIgnore
    // Use Roles as external API
    public Set<UserRoleEnum> getRoles() {
        Set<UserRoleEnum> roles = EnumSet.noneOf(UserRoleEnum.class);
        if (authorities != null) {
            for (UserAuthority authority : authorities) {
                roles.add(UserRoleEnum.valueOf(authority));
            }
        }
        return roles;
    }

    public void setRoles(Set<UserRoleEnum> roles) {
        for (UserRoleEnum role : roles) {
            grantRole(role);
        }
    }

    public void grantRole(UserRoleEnum role) {
        if (authorities == null) {
            authorities = new HashSet<UserAuthority>();
        }
        authorities.add(role.asAuthorityFor(this));
    }

    public void revokeRole(UserRoleEnum role) {
        if (authorities != null) {
            authorities.remove(role.asAuthorityFor(this));
        }
    }

    public boolean hasRole(UserRoleEnum role) {
        return authorities.contains(role.asAuthorityFor(this));
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String getUsername() {
        return username;
    }

    @Override
    @JsonIgnore
    public String getPassword() {
        return password;
    }

    @JsonProperty
    public void setPassword(String password) {
        this.password = password;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getEmail() {
        return email;
    }

    @JsonProperty
    public void setUserRole(UserRoleEnum userRole) {
        this.userRole = userRole;
    }

    @JsonIgnore
    public UserRoleEnum getUserRole() {
        return userRole;
    }

    public long getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(long createdOn) {
        this.createdOn = createdOn;
    }

    public long getUpdatedOn() {
        return updatedOn;
    }

    public void setUpdatedOn(long updatedOn) {
        this.updatedOn = updatedOn;
    }

    public User copyTo(User another){
        name = another.getName();
        return this;
    }

    @Override
    @JsonIgnore
    public boolean isAccountNonExpired() {
        return !accountExpired;
    }

    @Override
    @JsonIgnore
    public boolean isAccountNonLocked() {
        return !accountLocked;
    }

    @Override
    @JsonIgnore
    public boolean isCredentialsNonExpired() {
        return !credentialsExpired;
    }

    @Override
    @JsonIgnore
    public boolean isEnabled() {
        return !accountEnabled;
    }

    public long getExpires() {
        return expires;
    }

    public void setExpires(long expires) {
        this.expires = expires;
    }

    public Set<User> getSubscribers() {
        return subscribers;
    }

    public void setSubscribers(Set<User> subscribers) {
        this.subscribers = subscribers;
    }

    @Override
    public String toString() {
        return getClass().getSimpleName() + ": " + getUsername();
    }


    public void setAvatar(Document avatar) {
        this.avatar = avatar;
    }

    public Document getAvatar() {
        return avatar;
    }
}
