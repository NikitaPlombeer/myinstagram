package com.plombeer.instagram.entity.enums;


import com.plombeer.instagram.entity.User;
import com.plombeer.instagram.entity.UserAuthority;

public enum UserRoleEnum {

    ADMIN,
    USER;

    UserRoleEnum() {
    }

    public UserAuthority asAuthorityFor(final User user) {
        final UserAuthority authority = new UserAuthority();
        authority.setAuthority("ROLE_" + toString());
        authority.setUser(user);
        return authority;
    }

    public static UserRoleEnum valueOf(final UserAuthority authority) {
        switch (authority.getAuthority()) {
            case "ROLE_USER":
                return USER;
            case "ROLE_ADMIN":
                return ADMIN;
        }
        throw new IllegalArgumentException("No role defined for authority: " + authority.getAuthority());
    }
}
