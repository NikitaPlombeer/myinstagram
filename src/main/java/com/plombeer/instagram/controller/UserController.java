package com.plombeer.instagram.controller;

import com.plombeer.instagram.entity.User;
import com.plombeer.instagram.service.UserService;
import com.plombeer.instagram.utils.MessageResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.util.List;

/**
 * Created by Никита on 14.07.2016.
 */
@RequestMapping(value = "/api")
@RestController
public class UserController {

    @Autowired
    private UserService service;

    /**
     * @api {get} /api/me Получение текущего профиля
     * @apiName Получение текущего профиля
     * @apiGroup Users
     *
     * @apiSuccess {String} name Имя пользователя
     * @apiSuccess {String} username  Username пользователя
     * @apiSuccess {String} email  Email пользователя
     * @apiSuccess {Number} createdOn  Дата создания профиля
     * @apiSuccess {Number} updatedOn  Дата последнего обновления профиля
     * @apiSuccess {String} avatar Уникальный id аватара
     *
     * @apiSuccessExample Success-Response:
     *  HTTP/1.1 200 OK
     *  {
     *      "name": "Французов Никита",
     *      "username": "Plombeer",
     *      "email": "francuzov72@gmail.com",
     *      "createdOn": 1465290515157,
     *      "updatedOn": 1465290516661,
     *      "expires": 0,
     *      "avatar": "97d99bc8-ce9d-4a54-9764-ed4d8e1d1cef",
     *  }
     *
     */
    @RequestMapping(value = "/me", method = RequestMethod.GET, produces = "application/json;charset=UTF-8")
    @ResponseBody
    public User getMe(Principal principal) {
        return service.getByUsername(principal.getName());
    }

    @RequestMapping(value = "/users/like", method = RequestMethod.GET, produces = "application/json;charset=UTF-8")
    @ResponseBody
    public List<User> findUserLike(@RequestParam("text") String text){
        return service.findUserLike(text);
    }

    @RequestMapping(value = "/users/subscribe", method = RequestMethod.POST, produces = "application/json;charset=UTF-8")
    @ResponseBody
    public ResponseEntity<?> subscribe(@RequestBody User object, Principal principal) {
        User me = service.getByUsername(principal.getName());

        if(object.getUsername() != null)
            object = service.getByUsername(object.getUsername());
        else
            return new ResponseEntity<>(new MessageResponse("Необходимо передать"), HttpStatus.BAD_REQUEST);

        if(object == null)
            return new ResponseEntity<>(new MessageResponse("Пользователь не найден"), HttpStatus.NOT_FOUND);

        if(object.getUsername().equals(me.getUsername()))
            return new ResponseEntity<>(new MessageResponse("Ты дибил?"), HttpStatus.BAD_REQUEST);

        if(service.hasAlreadySubscribe(object.getUsername(), me.getUsername()))
            return new ResponseEntity<>(new MessageResponse("Вы уже подписаны"), HttpStatus.BAD_REQUEST);

        service.subscribe(object.getUsername(), me.getUsername());

        return new ResponseEntity<>(new MessageResponse("Подписка оформлена"), HttpStatus.OK);
    }

    @RequestMapping(value = "/users/unsubscribe", method = RequestMethod.POST, produces = "application/json;charset=UTF-8")
    @ResponseBody
    public ResponseEntity<?> unsubscribe(@RequestBody User object, Principal principal) {
        User me = service.getByUsername(principal.getName());

        if(object.getUsername() != null)
            object = service.getByUsername(object.getUsername());
        else
            return new ResponseEntity<>(new MessageResponse("Необходимо передать"), HttpStatus.BAD_REQUEST);

        if(object == null)
            return new ResponseEntity<>(new MessageResponse("Пользователь не найден"), HttpStatus.NOT_FOUND);

        if(object.getUsername().equals(me.getUsername()))
            return new ResponseEntity<>(new MessageResponse("Ты дибил?"), HttpStatus.BAD_REQUEST);

        if(!service.hasAlreadySubscribe(object.getUsername(), me.getUsername()))
            return new ResponseEntity<>(new MessageResponse("Вы не подписаны на данного пользователя"), HttpStatus.BAD_REQUEST);

        service.unsubscribe(object.getUsername(), me.getUsername());

        return new ResponseEntity<>(new MessageResponse("Подписка отозвана"), HttpStatus.OK);
    }

}
