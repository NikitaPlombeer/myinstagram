package com.plombeer.instagram.controller;

import com.plombeer.instagram.service.PhotoService;
import com.plombeer.instagram.service.UserService;
import com.plombeer.instagram.utils.FeedCard;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.math.BigInteger;
import java.security.Principal;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Никита on 15.07.2016.
 */
@RequestMapping(value = "/api")
@RestController
public class FeedController {


    @Autowired
    private UserService userService;

    @Autowired
    private PhotoService service;

    @RequestMapping(value = "/feed/{page}", method = RequestMethod.GET, produces = "application/json;charset=UTF-8")
    @ResponseBody
    public ResponseEntity<?> getMe(@PathVariable("page") int page, Principal principal) {
        List feed = service.getFeed(principal.getName(), page);
        List<FeedCard> cards = new ArrayList<>();
        for (Object o : feed) {
            Object[] arr = (Object[]) o;
            BigInteger createdOn = (BigInteger) arr[1];
            int likes = (int) arr[2];
            String text = (String) arr[3];
            String _username = (String) arr[4];
            String photo = (String) arr[5];

            FeedCard card = new FeedCard(_username, userService.getByUsername(_username).getAvatar().getId(), photo, text, likes, createdOn.longValue());
            cards.add(card);
        }
        return new ResponseEntity<>(cards, HttpStatus.OK);
    }
}
