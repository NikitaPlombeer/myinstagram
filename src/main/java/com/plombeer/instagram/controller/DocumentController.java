package com.plombeer.instagram.controller;

import com.plombeer.instagram.entity.Document;
import com.plombeer.instagram.service.DocumentService;
import com.plombeer.instagram.service.UserService;
import com.plombeer.instagram.utils.MessageResponse;
import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.OutputStream;
import java.sql.SQLException;

/**
 * Created by Никита on 15.07.2016.
 */
@RestController
public class DocumentController {

    @Autowired
    private DocumentService documentService;

    @Autowired
    private UserService userService;

    @RequestMapping(value = "/document/{documentId}", method = RequestMethod.GET)
    public ResponseEntity<?> getDocumentById(@PathVariable("documentId") String documentId, HttpServletResponse response) {
        Document doc = documentService.get(documentId);
        if(doc == null)
            return new ResponseEntity<>(new MessageResponse("No photo found"), HttpStatus.NOT_FOUND);

        createAvatarDownloadingResponse(response, doc);
        return null;
    }

    @RequestMapping(value = "/document/avatar/{username}", method = RequestMethod.GET)
    public ResponseEntity<?> getAvatarByUserId(@PathVariable("username") String username, HttpServletResponse response) {

        Document doc = userService.getByUsername(username).getAvatar();
        if(doc == null)
            return new ResponseEntity<>(new MessageResponse("No photo found"), HttpStatus.NOT_FOUND);

        createAvatarDownloadingResponse(response, doc);
        return null;
    }

    private void createAvatarDownloadingResponse(HttpServletResponse response, Document doc){
        try {
            response.setHeader("Content-Disposition", "inline;filename=\"" +doc.getCreated()+ "\"");
            OutputStream out = response.getOutputStream();
            response.setContentType(doc.getContentType());
            IOUtils.copy(doc.getContent().getBinaryStream(), out);
            out.flush();
            out.close();
        } catch (IOException | SQLException e) {
            e.printStackTrace();
        }
    }
}
