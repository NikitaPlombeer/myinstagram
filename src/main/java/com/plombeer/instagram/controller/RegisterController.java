package com.plombeer.instagram.controller;

import com.plombeer.instagram.entity.User;
import com.plombeer.instagram.entity.enums.UserRoleEnum;
import com.plombeer.instagram.service.UserService;
import com.plombeer.instagram.utils.MessageResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.encoding.ShaPasswordEncoder;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class RegisterController {

    @Autowired
    private UserService service;

    @Autowired
    private ShaPasswordEncoder shaPasswordEncoder;

    /**
     * @api {post} /register Регистрация пользователя
     * @apiName Регистрация
     * @apiGroup Users
     *
     * @apiSuccess {String} name Имя пользователя
     * @apiSuccess {String} username  Username пользователя
     * @apiSuccess {String} email  Email пользователя
     * @apiSuccess {Number} createdOn  Дата создания профиля
     * @apiSuccess {Number} updatedOn  Дата последнего обновления профиля
     *
     * @apiSuccessExample Success-Response:
     * HTTP/1.1 200 OK
     *  {
     *      "name": "Французов Никита",
     *      "username": "Plombeer",
     *      "email": "francuzov72@gmail.com",
     *      "createdOn": 1465290515157,
     *      "updatedOn": 1465290516661,
     *      "expires": 0,
     *      "avatar": null
     *  }
     *
     * @apiError InvalidData
     * @apiErrorExample InvalidData-Response: Необходимо ввести username, email, password
     * HTTP/1.1 400 Bad request
     *  {
     *      "message": "Необходимо ввести USERNAME, EMAIL, PASSWORD"
     *  }
     *
     * @apiError InvalidData
     * @apiErrorExample InvalidData-Response: Пользователь с таким username уже существует
     * HTTP/1.1 302 Found
     *  {
     *      "message": "Пользователь с таким username уже существует"
     *  }
     *
     * @apiError InvalidData
     * @apiErrorExample InvalidData-Response: Пользователь с таким email уже существует
     * HTTP/1.1 302 Found
     *  {
     *      "message": "Пользователь с таким email уже существует"
     *  }
     *
     * @apiError InvalidData
     * @apiErrorExample InvalidData-Response: Пароль не может быть меньше 6 символов
     * HTTP/1.1 400 Bad request
     *  {
     *      "message": "Пароль не может быть меньше 6 символов"
     *  }
     */
    @RequestMapping(value = "/register", method = RequestMethod.POST, produces = "application/json;charset=UTF-8")
    public ResponseEntity<?> register(@RequestBody User myUser) {
        String username = myUser.getUsername();
        String email = myUser.getEmail();
        String password = myUser.getPassword();

        if(username == null || email == null || password == null)
            return new ResponseEntity<>(new MessageResponse("Необходимо ввести USERNAME, EMAIL, PASSWORD"), HttpStatus.BAD_REQUEST);

        User user = service.getByUsername(username);
        if(user != null)
            return new ResponseEntity<>(new MessageResponse("Пользователь с таким username уже существует"), HttpStatus.FOUND);

        user = service.getUserByEmail(email);
        if(user != null)
            return new ResponseEntity<>(new MessageResponse("Пользователь с таким email уже существует"), HttpStatus.FOUND);

        if(password.length() < 6)
            return new ResponseEntity<>(new MessageResponse("Пароль не может быть меньше 6 символов"), HttpStatus.BAD_REQUEST);

        myUser.setPassword(shaPasswordEncoder.encodePassword(password, null));
        myUser.setUserRole(UserRoleEnum.USER);
        myUser.setCreatedOn(System.currentTimeMillis());
        myUser.setUpdatedOn(System.currentTimeMillis());

        User savedUser = service.save(myUser);
        return new ResponseEntity<>(savedUser, HttpStatus.OK);
    }

}
