package com.plombeer.instagram.controller;

import com.plombeer.instagram.entity.Document;
import com.plombeer.instagram.entity.Photo;
import com.plombeer.instagram.service.DocumentService;
import com.plombeer.instagram.service.PhotoService;
import com.plombeer.instagram.service.UserService;
import com.plombeer.instagram.utils.MessageResponse;
import org.hibernate.Hibernate;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.security.Principal;
import java.sql.Blob;
import java.sql.Date;

/**
 * Created by Никита on 14.07.2016.
 */
@RequestMapping(value = "/api")
@RestController
public class PhotoController {

    @Autowired
    private DocumentService documentService;

    @Autowired
    private PhotoService service;

    @Autowired
    private UserService userService;

    @Autowired
    private SessionFactory sessionFactory;

    @RequestMapping(value = "/files", method = RequestMethod.POST)
    public ResponseEntity<?> uploadFile(@RequestParam("file") MultipartFile file, @RequestParam("text") String text, Principal principal){
        System.out.println("File:" + file.getName());
        String contentType = file.getContentType();
        if(!contentType.startsWith("image/"))
            return new ResponseEntity<>(new MessageResponse("Your file isn't an image"), HttpStatus.NOT_MODIFIED);

        System.out.println("ContentType:" + contentType);
        Document document = new Document();

        try {
            byte[] bytes = file.getBytes();

            Session session = sessionFactory.openSession();
            session.beginTransaction();

            Blob image = Hibernate.getLobCreator(session).createBlob(bytes);

            session.getTransaction().commit();

            document.setContent(image);
            document.setContentType(contentType);
            document.setCreated(new Date(System.currentTimeMillis()));

            //Сохраняем документ в базу
            document = documentService.save(document);

            Photo photo = new Photo();
            photo.setAuthor(userService.getByUsername(principal.getName()));
            photo.setPhoto(document);
            photo.setCreatedOn(System.currentTimeMillis());
            photo.setText(text);

            service.save(photo);

            return new ResponseEntity<>(document, HttpStatus.OK);
        } catch (IOException e) {
            e.printStackTrace();
        }

        return new ResponseEntity<>(new MessageResponse("File was not upload"), HttpStatus.NOT_MODIFIED);
    }

}
