package com.plombeer.instagram.repository;

import com.plombeer.instagram.entity.Document;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

/**
 * Created by Никита on 14.07.2016.
 */
public interface DocumentRepository extends JpaRepository<Document, String> {

    @Query("SELECT d FROM Document d WHERE d.id = :id")
    Document getById(@Param("id")String id);
}
