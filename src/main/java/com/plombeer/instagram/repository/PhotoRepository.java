package com.plombeer.instagram.repository;

import com.plombeer.instagram.entity.Photo;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by Никита on 15.07.2016.
 */
public interface PhotoRepository extends JpaRepository<Photo, String>{
}
