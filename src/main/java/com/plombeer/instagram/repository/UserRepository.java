package com.plombeer.instagram.repository;

import com.plombeer.instagram.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

/**
 * Created by Никита on 14.07.2016.
 */
public interface UserRepository extends JpaRepository<User, String>{
    User getUserByEmail(String email);

    @Query("SELECT u FROM User u WHERE username LIKE %:textLike%")
    List<User> findUsersLike(@Param("textLike")String textLike);
}
