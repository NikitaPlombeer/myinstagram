package com.plombeer.instagram.service;

import com.plombeer.instagram.entity.Document;

/**
 * Created by Никита on 14.07.2016.
 */
public interface DocumentService {

    Document save(Document document);
    Document get(String id);
    void remove(String id);
}
