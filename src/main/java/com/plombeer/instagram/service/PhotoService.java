package com.plombeer.instagram.service;


import com.plombeer.instagram.entity.Photo;

import java.util.List;

/**
 * Created by Никита on 15.07.2016.
 */
public interface PhotoService {

    Photo save(Photo document);
    Photo get(String id);
    void remove(String id);

    List<Photo> getFeed(String username, int page);
}
