package com.plombeer.instagram.service.impl;

import com.plombeer.instagram.entity.Photo;
import com.plombeer.instagram.repository.PhotoRepository;
import com.plombeer.instagram.service.PhotoService;
import com.plombeer.instagram.utils.FeedCard;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import java.util.List;

/**
 * Created by Никита on 15.07.2016.
 */
@Service
public class PhotoServiceImpl implements PhotoService{

    @Autowired
    EntityManager entityManager;

    @Autowired
    PhotoRepository repository;

    @Override
    public Photo save(Photo document) {
        return repository.saveAndFlush(document);
    }

    @Override
    public Photo get(String id) {
        return repository.getOne(id);
    }

    @Override
    public void remove(String id) {
        repository.delete(id);
    }

    @Override
    public List<Photo> getFeed(String username, int page) {
        List feed = entityManager.createNamedQuery("getFeed").setParameter(1, username).setParameter(2, page).getResultList();
        return feed;
    }
}
