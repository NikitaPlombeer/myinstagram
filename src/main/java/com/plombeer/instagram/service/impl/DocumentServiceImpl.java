package com.plombeer.instagram.service.impl;

import com.plombeer.instagram.entity.Document;
import com.plombeer.instagram.repository.DocumentRepository;
import com.plombeer.instagram.service.DocumentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by Никита on 14.07.2016.
 */
@Service
public class DocumentServiceImpl implements DocumentService{

    @Autowired
    DocumentRepository repository;

    @Override
    public Document save(Document document) {
        return repository.saveAndFlush(document);
    }

    @Override
    public Document get(String id) {
        return repository.getById(id);
    }

    @Override
    public void remove(String id) {
        repository.delete(id);
    }
}
