package com.plombeer.instagram.service.impl;

import com.plombeer.instagram.entity.User;
import com.plombeer.instagram.repository.UserRepository;
import com.plombeer.instagram.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.math.BigInteger;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by Никита on 04.06.2016.
 */
@Service
public class UserServiceImpl implements UserService {

    @Autowired
    UserRepository repository;

    @Autowired
    EntityManager entityManager;

    @Override
    public List<User> getAll() {
        return repository.findAll();
    }

    @Override
    public List<User> findUserLike(String like) {
        return repository.findUsersLike(like);
    }

    @Override
    public User getByUsername(String username) {
        return repository.findOne(username);
    }

    @Override
    public User save(User item) {
        return repository.saveAndFlush(item);
    }

    @Override
    public void remove(String username) {
        repository.delete(username);
    }

    @Override
    public User getUserByEmail(String email) {
        return repository.getUserByEmail(email);
    }

    @Override
    public Set<User> getSubscriptions(String username) {
        List getAntiSub = entityManager.createNamedQuery("getSubscriptions").setParameter(1, username).getResultList();
        Set<User> userSet = new HashSet<>();
        for (Object o : getAntiSub) {
            Object oo[] = (Object[]) o;
            User user = getByUsername((String) oo[0]);
            userSet.add(user);
        }
        return userSet;
    }

    @Override
    public Set<User> getSubscribers(String username) {
        List getAntiSub = entityManager.createNamedQuery("getSubscribers").setParameter(1, username).getResultList();
        Set<User> userSet = new HashSet<>();
        for (Object o : getAntiSub) {
            Object oo[] = (Object[]) o;
            User user = getByUsername((String) oo[1]);
            userSet.add(user);
        }
        return userSet;
    }

    @Transactional
    @Override
    public boolean subscribe(String username, String subscriber_username) {
        int subscribe = entityManager.createNamedQuery("subscribe").setParameter(1, username).setParameter(2, subscriber_username).executeUpdate();
        return true;
    }

    @Override
    public boolean hasAlreadySubscribe(String username, String subscriber_username) {
        List hasAlreadySubscribe = entityManager.createNamedQuery("hasAlreadySubscribe").setParameter(1, username).setParameter(2, subscriber_username).getResultList();
        BigInteger count = (BigInteger) hasAlreadySubscribe.get(0);
        return !count.equals(new BigInteger("0"));
    }

    @Transactional
    @Override
    public void unsubscribe(String username, String subscriber_username) {
        entityManager.createNamedQuery("unsubscribe").setParameter(1, username).setParameter(2, subscriber_username).executeUpdate();
    }

}
