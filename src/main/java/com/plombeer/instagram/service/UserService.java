package com.plombeer.instagram.service;


import com.plombeer.instagram.entity.User;

import java.util.List;
import java.util.Set;

/**
 * Created by Никита on 04.06.2016.
 */
public interface UserService {
    List<User> getAll();
    List<User> findUserLike(String like);

    Set<User> getSubscriptions(String username);
    Set<User> getSubscribers(String username);

    User getByUsername(String username);
    User save(User item);
    User getUserByEmail(String email);

    void remove(String username);
    void unsubscribe(String username, String subscriber_username);

    boolean subscribe(String username, String subscriber_username);
    boolean hasAlreadySubscribe(String username, String subscriber_username);
}
