package com.plombeer.instagram.utils;

/**
 * Created by Никита on 15.07.2016.
 */
public class FeedCard {

    private String username;
    private String avatar;
    private String imageUrl;
    private String text;
    private int likes;
    private long createdOn;

    public FeedCard(String username, String avatar, String imageUrl, String text, int likes, long createdOn) {
        this.username = username;
        this.avatar = avatar;
        this.imageUrl = imageUrl;
        this.text = text;
        this.likes = likes;
        this.createdOn = createdOn;
    }

    public String getUsername() {
        return username;
    }

    public String getAvatar() {
        return avatar;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public String getText() {
        return text;
    }

    public int getLikes() {
        return likes;
    }

    public long getCreatedOn() {
        return createdOn;
    }
}
